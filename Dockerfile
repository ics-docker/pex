FROM centos:7.6.1810

LABEL maintainer "benjamin.bertrand@esss.se"

# Install common dependencies
RUN  yum -y update \
  && yum -y install \
     gcc \
     make \
     python-devel \
     python-setuptools \
     python-virtualenv \
     libffi-devel \
     openssl-devel \
  && yum clean all \
  && easy_install pip \
  && pip install --upgrade setuptools

WORKDIR /build

# Install pex
RUN virtualenv pex \
  && source pex/bin/activate \
  && pip install pex \
  && pex pex requests -c pex -o /usr/local/bin/pex \
  && deactivate \
  && rm -rf pex
