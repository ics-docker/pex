Dockerfile to build a PEX docker image
======================================

This Dockerfile creates a Docker_ image to build .pex files.

PEX_ files are self-contained executable Python virtual environments. PEX files
make it easy to deploy Python applications.

Building
--------

This image is built automatically by GitLab.

How to use this image
---------------------

For example to create an ansible-playbook pex file::

    $ docker run --rm -v $(pwd):/build registry.esss.lu.se/ics-docker/pex:latest pex ansible==2.3.0.0 -c ansible-playbook -o ansible-playbook


See https://pex.readthedocs.io/en/stable/buildingpex.html for more information.


.. _Docker: https://www.docker.com
.. _PEX: https://pex.readthedocs.io/en/stable/index.html
